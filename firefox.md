## Useful Firefox about:config tweaks

A list of settings in Firefox that enable hidden or upcoming features and are not editable from main Settings page. These may or may not work with Firefox for Android.

In the list, the paperclip emoji links a relevant extension, fox emoji states a minimum Firefox version required to enable the setting.

Want a quick generator of some relevant settings? Check out [ffprofile.com](https://ffprofile.com/#start)!

I also recommend [Techdows](https://techdows.com/category/firefox) as they also cover new features.

Looking for UI changes? Try out some [UserStyles for Firefox](https://gitlab.com/Madis0/Firefox-tweaks)!

### How to install
1. Open `about:config`
2. Accept the warning
3. Search for the setting name (if not present, create with right click)
4. Double-click to set a new value
5. Reload affected pages or restart browser

### Tabs

* [Adjust tab's minimum width in pixels](https://www.reddit.com/r/firefox/comments/74983i/new_minwidth_default_in_todays_nightly/) - **browser.tabs.tabMinWidth** = 50-200
* [Switch between tabs with a scroll](https://www.reddit.com/r/firefox/comments/a3mrpa/firefox_65_developer_editionbeta_finally_gets/) - **toolkit.tabbox.switchByScrolling** = true
* [Close tabs with a double click](https://blog.mozilla.org/addons/2018/05/17/extensions-in-firefox-61/) - **browser.tabs.closeTabByDblclick** = true

### Features being tested (may get removed)

* [Write "Not Secure" on HTTP pages (like Chrome)](https://bugzilla.mozilla.org/show_bug.cgi?id=1335970) - **security.insecure_connection_text.enabled** = true

### Privacy and security [📎](https://addons.mozilla.org/en-US/firefox/collections/tulirebane/security-privacy/) ([Mozilla's page](https://developer.mozilla.org/en-US/Firefox/Experimental_features#Security))

* [Enforce HTTPS connections for all sites, HTTP will show an error](https://bugzilla.mozilla.org/show_bug.cgi?id=1613063) - **dom.security.https_only_mode** = true
* [Upgrade all possible HTTP media in HTTPS sites, block the rest](https://bugzilla.mozilla.org/show_bug.cgi?id=1435733) - **security.mixed_content.upgrade_display_content** = true [📎](https://addons.mozilla.org/en-US/firefox/addon/upgrademixedcontent/)
* [Block all HTTP media in HTTPS sites (alternative to option above)](https://blog.mozilla.org/tanvi/2013/04/10/mixed-content-blocking-enabled-in-firefox-23/) - **security.mixed_content.block_display_content** = true
* [Mark sites with unsafe negotiation as mixed content](https://bugzilla.mozilla.org/show_bug.cgi?id=665859) - **security.ssl.treat_unsafe_negotiation_as_broken** = true
* [Send referer values only in same domain](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy#Browser_compatibility) - **network.http.referer.userControlPolicy** = 1 [📎](https://addons.mozilla.org/en-US/firefox/addon/smart-referer/)
* [Prevent page hijacking on external links](https://bugzilla.mozilla.org/show_bug.cgi?id=1503681) - **dom.targetBlankNoOpener.enabled** = true [📎](https://addons.mozilla.org/et/firefox/addon/dont-touch-my-tabs/)
* [SameSite=lax cookies (Chrome parity)](https://bugzilla.mozilla.org/show_bug.cgi?id=1604212) - **network.cookie.sameSite.laxByDefault** = true

### More privacy with encrypted DNS and ESNI

[DNS](https://en.wikipedia.org/wiki/Domain_Name_System) is a system that converts domains like [example.com](https://example.com) to [IP-addresses](https://en.wikipedia.org/wiki/IP_address) like `93.184.216.34` or `2606:2800:220:1:248:1893:25c8:1946`.
While your internet service provider may do that just fine, it could potentially censor sites you visit and sometimes be slower than an external DNS.
Besides, most systems currently use DNS unencrypted, so it shares too much information with your DNS provider. Hence, it would be wise to use an encrypted DNS provider.

Firefox also supports [encrypted SNI](https://blog.mozilla.org/security/2018/10/18/encrypted-sni-comes-to-firefox-nightly/) which makes sure that other people in the same network can not see what domains you visit, if you visit one of the sites that support this feature.

Here's how to enable both:

1. Open menu > **Preferences > Network Settings > Settings... > Enable DNS over HTTPS**
2. Open `about:config` and set **network.security.esni.enabled** = true
3. [Test your browser](https://www.cloudflare.com/ssl/encrypted-sni/) to see if you have set things up correctly.

### Firefox services
Services by Mozilla that are enabled by default, but can be disabled if you prefer.

* [Pocket](https://help.getpocket.com/article/942-where-is-the-pocket-button-in-firefox) - **extensions.pocket.enabled** = false
* [Firefox Screenshots](https://support.mozilla.org/en-US/kb/firefox-screenshots) - **extensions.screenshots.disabled** = true

### User choice
Settings that are always disabled by default, but can be enabled if you consider them useful.

* [Show HTTP in URL](http://jj.isgeek.net/2011/08/show-http-protocol-in-url-bar-for-firefox-7/) - **browser.urlbar.trimURLs** = false
* Hide HTTPS in URL (shows HTTP only) - **browser.urlbar.trimHttps** = true
* [Show URLs always with latin letters](https://www.wordfence.com/blog/2017/04/chrome-firefox-unicode-phishing/) - **network.IDN_show_punycode** = true
* [Disable backspace as "go back" action](http://kb.mozillazine.org/Browser.backspace_action) - **browser.backspace_action** = 2
* [Open popups always on new tabs](https://support.mozilla.org/et/questions/1199017#answer-1062434) - **browser.link.open_newwindow.restriction** = 0
* [Open bookmarks on new tabs](https://winaero.com/blog/open-bookmarks-always-new-tab-firefox/) - **browser.tabs.loadBookmarksInTabs** = true
* [Sync any preference across devices (may not always work)](https://www.reddit.com/r/firefox/comments/756gqf/) - **services.sync.prefs.sync.*preference_name*** = true
* [Enable userChrome.css customizations](https://www.reddit.com/r/firefox/comments/brttne/psa_firefox_v69_users_will_have_to_set_a_pref_to/) - **toolkit.legacyUserProfileCustomizations.stylesheets** = true
* [Enable extensions on addons.mozilla.org](https://bugzilla.mozilla.org/show_bug.cgi?id=1310082#c24) (potential security risk) - 
  * set **privacy.resistFingerprinting.block_mozAddonManager** = true 
  * remove `addons.mozilla.org` from **extensions.webextensions.restrictedDomains**
  * or [just add a period to the domain](https://www.reddit.com/r/firefox/comments/cxnbdb/why_is_it_that_certain_mozilla_sites_are_treated/eym9bbc/)

## Even more tweaks

More tweaks can be obtained by using enterprice policies, as [described here](https://support.mozilla.org/en-US/kb/customizing-firefox-using-policiesjson).
[List of policies can be found here](https://github.com/mozilla/policy-templates/blob/master/README.md) and a [generator extension is here](https://addons.mozilla.org/en-US/firefox/addon/enterprise-policy-generator/).
