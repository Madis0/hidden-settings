## Useful Chrome chrome://flags tweaks

A list of tweaks in Chrome (Chromium) that enable hidden or upcoming features and are not editable from the main settings page. Some flags may require two restarts (`chrome://restart`).

Since I don't follow the tweaks [by bugs](https://bugs.chromium.org/p/chromium/issues/list), I am not always sure, what version of Chromium actually shows them. Therefore, I recommend using at least Beta version of the browser, if not Dev or Canary.

Flags can and will be eventually removed. [See this document](https://chromium.googlesource.com/chromium/src/+/master/docs/flag_expiry.md) and read "Disable/enable more features on desktop" for permanent flags.

Looking for even more tweaks? Try out some [UserStyles for Vivaldi](https://gitlab.com/Madis0/Vivaldi-tweaks)! (Chrome does not support them.)

I also recommend [Techdows](https://techdows.com/category/google-chrome) as they cover new flags a lot.

### How to enable

1. Open `chrome://flags` (`vivaldi://flags` on Vivaldi, `browser://flags` on Yandex, etc.)
2. Search for the hashtag title
3. Set the recommended setting
4. Press RELAUNCH NOW to restart the browser

Alternatively, set them as "target" values (read below).

### Undo user-hostile changes

* [Show correct colors in webpages](https://stackoverflow.com/a/46220918) - **#force-color-profile** = sRGB
* Show the correct URL in search pages - **#enable-query-in-omnibox** = Disabled
* Prevent websites from detecting incognito mode - **#enable-filesystem-in-incognito** = Enabled
* Bring back some removed flags (temporarily and may not work) - **#temporary-unexpire-flags-m*XX*** = Enabled

### Privacy and security

* Highlight insecure sites in red - **#enable-mark-http-as** = Enabled (mark as actively dangerous)
* Freeze the user agent (less fingerprinting) - **#freeze-user-agent** = Enabled
* Show "not secure" on old TLS - **#show-legacy-tls-warnings** = Enabled
* Show "not secure" on mixed content - **#passive-mixed-content-warning** = Enabled
* Block executable files over HTTP -  **#treat-unsafe-downloads-as-active-content** = Enabled
* Send referer values only in same domain - **#reduced-referrer-granularity** = Enabled
* Limit most cookies to same site - **#same-site-by-default-cookies** = Enabled
* Disable JS temporary on slow networks - **#enable-noscript-previews** = Enabled
* Limit every site to a separate process (better security, but more RAM used) - **#strict-origin-isolation** = Enabled
* Show mixed content "unblocking" as a site setting - **#mixed-content-setting** = Enabled

### Desktop tabs

* Tab groups (horizontal) - **#tab-groups** = Enabled
* New loading animation - **#new-tab-loading-animation** = Enabled
* Switch tabs by scrolling the tabbar - **#scrollable-tabstrip** = Enabled
* Show tab title and domain on hover - **#tab-hover-cards** = Enabled
* Show tab thumbnail on hover (requires previous flag) - **#tab-hover-card-images** = Enabled

### Desktop design

* Make pulling the page from the top (with touch) reload it - **#pull-to-refresh** = Enabled
* Enable reader mode for articles - **#enable-reader-mode** = Enabled
* Special menu for extensions (Firefox-like) - **#extensions-toolbar-menu** = Enabled
* Control playing media from any tab with a toolbar button - **#global-media-controls** = Enabled
* Mouse-closable full screen UI - **#enable-experimental-fullscreen-exit-ui** = Enabled
* Overlay scrollbars - **#overlay-scrollbars** = Enabled
* More rounded corners on dialogs - **#enable-md-rounded-corners-on-dialogs** = Enabled
* Make page scrolling smoother - **#smooth-scrolling** = Enabled


### Android-only

Tabs
* Tabs in a grid (like current mobile Firefox) - **#enable-tab-grid-layout** = Enabled
* Tab groups (+ enables tabs in a grid) - **#enable-tab-groups** = Enabled
* Tab groups improvements (groups are shown on tab list, tabs are mergable and draggable) - **#enable-tab-groups-ui-improvements** = Enabled
* Horizontal tabs (similar to Android 9 but smaller) - **#enable-horizontal-tab-switcher** = Enabled
* Tab list without thumbnails - **#enable-accessiblity-tab-switcher** = Enabled
* Show tabs after not using the browser for a while - **#enable-tab-switcher-on-return** = Enabled x minutes
* Add a hold menu to tab list - **#tab-switcher-longpress-menu** = Enabled

Dark mode
* Dark interface (enable in settings) - **#enable-android-night-mode** = Enabled
* Dark pages - **#enable-android-web-contents-dark-mode** = Enabled

Misc
* Make notification prompt less annoying - **#quiet-notification-prompts** = Enabled (mini-infobars)
* Show a snackbar when a download is in progress - **#download-progress-infobar** = Enabled
* Add a "sneak peak" menu option for links - **#ephemeral-tab** = Enabled
* Dynamic context menu - **#enable-custom-context-menu** = Enabled
* Navigate back/forward with an edge swipe - **#overscroll-history-navigation** = Enabled
* Enter/exit fullscreen video on rotation - **#video-rotate-to-fullscreen** = Enabled
 
### Extra button in toolbar

Chrome has a home button that replaces the current page with a page you define. The interesting part about this is that you can use scripts in it to execute various actions in the page. This is especially useful in the mobile app where you can't add extra buttons by extensions.

On desktop:
1. Press the vertical ellipsis (**⋮**) button
2. Click on **Settings**
3. Scroll to **Show home button**
4. Click on it and set the address accordingly (see below)

On mobile:
1. Go to **chrome://flags**
2. Enable **#force-enable-home-page-button**
3. Restart browser twice (type **chrome://restart** to address bar)
4. Press the vertical ellipsis (**⋮**) button
5. Click on **Settings**
6. Scroll to **Home page**
7. Set **open this page** accordingly (see below)

Useful scripts to try:
* Back - `javascript:history.back();`
* Forward - `javascript:history.forward();`
* Reload - `javascript:location.reload();`
* New tab (can replace _about:blank_ with any URL) - `javascript:window.open('about:blank');`
* Close tab (rarely works) - `javascript:window.close();`
* [More bookmarklets here](https://www.hongkiat.com/blog/100-useful-bookmarklets-for-better-productivity-ultimate-list/) (note that the list is old, so not all may work today)
 
### Disable/enable more features on desktop

On desktop browsers, it is possible to disable and enable even more features than seen in `chrome://flags`.

To do that:

1. Find the icon you launch your browser from and [change it's "target" arguments](https://www.chromium.org/developers/how-tos/run-chromium-with-flags). (Alt instructions: [Windows](https://superuser.com/q/29569), [Mac](https://superuser.com/q/271678), [Linux](https://stackoverflow.com/q/8850938))
2. To the end of the path, add the target argument `--disable-features=` and/or `--enable-features=`. Make sure the `--` arguments are separated by spaces and are added before `--flag-switches-begin` and `--flag-switches-end`
3. After the equals sign, add the arguments, separated by commas (no spaces here!)
4. Confirm they work by clicking the icon and viewing `chrome://version/` Target area.

To find features to add, [search here](https://cs.chromium.org/search/?q=_features.cc&p=1&sq=package:chromium&type=cs) for files ending with `_features.cc`.

Inside the files you'll find lines like `const base::Feature kDockedMagnifier{"DockedMagnifier", base::FEATURE_ENABLED_BY_DEFAULT};` - there the quoted part is the feature name.

For example, I suggest using `disable-features=OmniboxUIExperimentHideSteadyStateUrlTrivialSubdomains,OmniboxUIExperimentSwapTitleAndUrl,QueryInOmnibox`
