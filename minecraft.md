## Useful Minecraft options.txt tweaks

A list of settings in Minecraft that enable hidden or upcoming features and are not editable from main Options page. This one refers only to the desktop Java Edition.

More info from [official wiki](https://minecraft.gamepedia.com/Options.txt).

### How to install
1. Open default `.minecraft` folder
    * Windows 	`%APPDATA%\.minecraft`
    * macOS 	`~/Library/Application Support/minecraft`
    * Linux 	`~/.minecraft`
2. Open `options.txt` with preferred text editor
3. Edit the value you want
4. Save and open Minecraft

### More than default

* Full brightness - **gamma**:15.0
* Wider chat - **chatWidth**:1.75 (for full screen 1600 X 900)

### Annoyance fixes

* Skip tutorial - **tutorialStep**:none
* Hide hotbar block names - **heldItemTooltips**:false
* Don't pause the game when focus is lost - **pauseOnLostFocus**:false (F3 + P)

### Extras

* Show block ID, durability and other info on tooltips - **advancedItemTooltips**:true (F3 + H)


